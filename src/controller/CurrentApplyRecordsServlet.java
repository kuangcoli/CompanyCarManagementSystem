package controller;

import modle.Bean.ApplyCarRecord;
import modle.Bean.User;
import modle.Service.ApplyCarService;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serial;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet("/CurrentApply")
public class CurrentApplyRecordsServlet extends HttpServlet {
    @Serial
    private static final long serialVersionUID = 9L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ApplyCarService applyCarService = new ApplyCarService();
        int employeeID = Integer.parseInt(request.getParameter("employeeID"));
        Map<String, Object> result = new HashMap<>();
        List<ApplyCarRecord> currentApplyCarRecords= applyCarService.getCurrentApplyCarRecords(employeeID);
        result.put("code", 0);
        result.put("msg", "请求成功");
        result.put("count", currentApplyCarRecords.size());
        result.put("data", currentApplyCarRecords);
        JSONObject json = new JSONObject(result);
        String jsonStr = json.toString();
        response.setContentType("text/javascript;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.write(jsonStr);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
