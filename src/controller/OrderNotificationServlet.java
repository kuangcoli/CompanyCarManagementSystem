package controller;

import modle.Bean.OrderNotification;
import modle.Service.OrderService;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serial;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @className: OrderNotificationServlet
 * @description: 订单通知的servlet
 * @author: 匡科利
 * @date: 2023/05/29 16:03
 * @Company: Copyright© [日期] by [作者或个人]
 **/
@WebServlet("/OrderNotification")
public class OrderNotificationServlet extends HttpServlet {
    @Serial
    private static final long serialVersionUID = 13L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        OrderService orderService = new OrderService();
        int employeeID = Integer.parseInt(request.getParameter("employeeID"));
        Map<String, Object> result = new HashMap<>();
        List<OrderNotification> orderNotifications = orderService.getOrderNotifications(employeeID);
        result.put("code", 0);
        result.put("msg", "请求成功");
        result.put("count", orderNotifications.size());
        result.put("data", orderNotifications);
        JSONObject json = new JSONObject(result);
        String jsonStr = json.toString();
        response.setContentType("text/javascript;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.write(jsonStr);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
