package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modle.Bean.Employee;
import modle.DAO.UserDao;
import modle.Bean.JDBCUtil;
import modle.Bean.User;

/**
 * 登录的 Servlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // 接收前台传来的值 账号和密码
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String roletype = request.getParameter("roletype");
        String inputcode = request.getParameter("inputcode");
        String imgcode = request.getParameter("code");
        System.out.println(inputcode);
        System.out.println(imgcode);
        response.setContentType("text/javascript;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<script src='https://cdn.bootcss.com/jquery/3.4.1/jquery.min.js'></script>");
        out.println("<link rel='stylesheet' href='https://unpkg.com/layui-layer/dist/layer.css'>");
        out.println("<script src='https://unpkg.com/layui-layer/dist/layer.js'></script>");
        // 账号密码登录的方式
        if(inputcode.equals(imgcode) ){
            if(username != null && password != null) {
                //解决中文字符乱码
                byte[] bytes = username.getBytes(StandardCharsets.ISO_8859_1);
                username = new String(bytes, StandardCharsets.UTF_8);
                JDBCUtil db = new JDBCUtil();
                // 创建一个用户保存下将密码和用户名保存
                User user = new User(username,password,roletype);
                UserDao userDao = new UserDao();
                try {
                    //数据库连接
                    Connection conn = db.getConn();
                    if(userDao.login(conn, user) != null) {
                        HttpSession session = request.getSession();
                        session.setAttribute("user", user);
                        session.removeAttribute("errorMsg");
                        String userrole = user.getRoletype();
                        switch (userrole) {
                            case "employee":
                                Employee employee = new Employee(user.getUsername(),user.getPassword(),user.getRoletype());
                                userDao.employeeInfo(conn,employee);
                                session.setAttribute("employee", employee);
                                response.sendRedirect("./jsp/empl/employee_index.jsp");
                                break;
                            case "driver":
                                response.sendRedirect("./jsp/driver/driver_index.jsp");
                                break;
                            case "vehicleManager":
                                response.sendRedirect("./jsp/vehicleManager/vm_index.jsp");
                                break;
                            case "deptAdmin":
                                response.sendRedirect("");
                                break;
                            case "companyAdmin":
                                response.sendRedirect("./jsp/companyAdmin/companyAdmin_index.jsp");
                                break;
                        }


                    } else {
                        request.setAttribute("errorMsg", "用户名或密码错误！");
                        RequestDispatcher rd = request.getRequestDispatcher( "/index.jsp");
                        rd.forward(request, response);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    out.close();
                }
            }
        }
        else {
            request.setAttribute("errorMsg", "验证码错误！");
            RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
            rd.forward(request, response);
        }



    }

}
