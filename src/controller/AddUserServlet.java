package controller;

import modle.Bean.JDBCUtil;
import modle.Bean.User;
import modle.DAO.UserDao;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serial;
import java.sql.Connection;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet("/AddUser")
public class AddUserServlet extends HttpServlet {
    @Serial
    private static final long serialVersionUID = 7L;
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String username = request.getParameter("username");
        String confrim_password = request.getParameter("confrim_password");
        String password = request.getParameter("password");
        String roletype = request.getParameter("roletype");
        String real_name = request.getParameter("real_name");
        String tel = request.getParameter("tel");
        String regist_date = now.format(formatter);
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setRegist_date(regist_date);
        user.setTel(tel);
        user.setReal_name(real_name);
        user.setRoletype(roletype);
        JDBCUtil db = new JDBCUtil();
        Connection conn = db.getConn();
        response.setContentType("text/javascript;charset=UTF-8");
        UserDao userDao = new UserDao();
        try {
            if (confrim_password == password || confrim_password.equals(password)) {
                userDao.addUserInfo(conn,user);
//               添加完一个用户重新获取所有用户的列表以更新列表
                Map<String, Object> result = new HashMap<>();
                List<User> userList = userDao.getAllUsers(conn);  // 获取所有用户数据
                result.put("code", 0);
                result.put("msg", "请求成功");
                result.put("count", userList.size());
                result.put("data", userList);
                // 将userList数据转换为json格式数据
                JSONObject json = new JSONObject(result);
                String jsonStr = json.toString();
                try (PrintWriter out = response.getWriter()) {
                    out.write(jsonStr);
                    out.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }



        } catch (Exception e) {

            throw new RuntimeException(e);
        }

    }
}
