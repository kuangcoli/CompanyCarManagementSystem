package controller;

import modle.Bean.ApplyCarRecord;
import modle.Service.ApplyCarService;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serial;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet("/AllApply")
public class AllApplyRecordsServlet extends HttpServlet {
    @Serial
    private static final long serialVersionUID = 10L;
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ApplyCarService applyCarService = new ApplyCarService();
        int employeeID = Integer.parseInt(request.getParameter("employeeID"));
        Map<String, Object> result = new HashMap<>();
        List<ApplyCarRecord> AllApplyCarRecords= applyCarService.getAllApplyCarRecords(employeeID);
        result.put("code", 0);
        result.put("msg", "请求成功");
        result.put("count", AllApplyCarRecords.size());
        result.put("data", AllApplyCarRecords);
        // 将userList数据转换为json格式数据
        JSONObject json = new JSONObject(result);
        String jsonStr = json.toString();
        response.setContentType("text/javascript;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.write(jsonStr);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
