package controller;

import modle.Bean.ApplyCarForm;
import modle.Bean.ApplyCarRecord;
import modle.Bean.Employee;
import modle.Service.ApplyCarService;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serial;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @className: DeleteApplyCarServlet
 * @description: 用于撤销当前未结束的用车申请 并获取所有未结束的用车申请重载表格
 * @author: 匡科利
 * @date: 2023/05/25 11:01
 **/
@WebServlet("/DeleteApplyCar")
public class DeleteApplyCarServlet extends HttpServlet {
    @Serial
    private static final long serialVersionUID = 11L;
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取前台传来的用车申请表数据 并构建实体后调用service方法 传入参数后删除该条记录  最后重新获取所有申请状态未结束的数据
        int apply_status = Integer.parseInt(request.getParameter("applyStatus"));
        int employeeID = Integer.parseInt(request.getParameter("employeeID"));
        String apply_date = request.getParameter("apply_date");
        String start_date = request.getParameter("start_date");
        String start_location = request.getParameter("start_location");
        String destination = request.getParameter("destination");
        ApplyCarForm applyCarForm = new ApplyCarForm();
        Employee applicant = new Employee();
        applicant.setEmployeeID(employeeID);
        applyCarForm.setApplicant(applicant);
        applyCarForm.setApply_date(apply_date);
        applyCarForm.setStart_date(start_date);
        applyCarForm.setStart_location(start_location);
        applyCarForm.setDestination(destination);
        ApplyCarService applyCarService = new ApplyCarService();
        if (apply_status == 0){
            try {
                applyCarService.deletApplyCar(applyCarForm);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            Map<String, Object> result = new HashMap<>();
            List<ApplyCarRecord> currentApplyCarRecords= applyCarService.getCurrentApplyCarRecords(employeeID);
            result.put("code", 0);
            result.put("msg", "请求成功");
            result.put("count", currentApplyCarRecords.size());
            result.put("data", currentApplyCarRecords);
            // 将userList数据转换为json格式数据
            JSONObject json = new JSONObject(result);
            String jsonStr = json.toString();
            response.setContentType("text/javascript;charset=UTF-8");
            try (PrintWriter out = response.getWriter()) {
                out.write(jsonStr);
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            try {
                applyCarService.deletApplyCar(applyCarForm);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            Map<String, Object> result = new HashMap<>();
            List<ApplyCarRecord> allApplyCarRecords= applyCarService.getAllApplyCarRecords(employeeID);
            result.put("code", 0);
            result.put("msg", "请求成功");
            result.put("count", allApplyCarRecords.size());
            result.put("data", allApplyCarRecords);
            // 将userList数据转换为json格式数据
            JSONObject json = new JSONObject(result);
            String jsonStr = json.toString();
            response.setContentType("text/javascript;charset=UTF-8");
            try (PrintWriter out = response.getWriter()) {
                out.write(jsonStr);
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
