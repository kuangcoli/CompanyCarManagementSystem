package controller;

import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.Serial;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modle.DAO.UserDao;
import modle.Bean.JDBCUtil;
import modle.Bean.User;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
/**
 * 注册的 Servlet
 */
@WebServlet("/RegisterUser")
public class RegisterUserServlet extends HttpServlet {
    @Serial
    private static final long serialVersionUID = 3L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
// 接收前台传来的值 账号和密码
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String roletype = request.getParameter("roletype");
        String tel = request.getParameter("tel");
        String real_name = request.getParameter("real_name");
        String confrim_password = request.getParameter("confrim_password");
        response.setContentType("text/javascript;charset=UTF-8");
        if (confrim_password.equals(password)) {
            if (username != null && password != null) {
                byte[] bytes = username.getBytes(StandardCharsets.ISO_8859_1);
                username = new String(bytes, StandardCharsets.UTF_8);
                JDBCUtil db = new JDBCUtil();
                // 创建一个用户保存下将密码和用户名保存
                User user = new User(username, password, roletype);
                user.setTel(tel);
                user.setReal_name(real_name);
                System.out.println(user.getTel());
                System.out.println(user.getReal_name());
                LocalDateTime now = LocalDateTime.now();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                String regist_date = now.format(formatter);
                user.setRegist_date(regist_date);

                UserDao userDao = new UserDao();
                try {
                    //数据库连接
                    Connection conn = db.getConn();
                    if (userDao.login(conn, user) == null) {
                        HttpSession session = request.getSession();
                        session.setAttribute("user", user);
                        session.removeAttribute("errorMsg");
                        userDao.addUser(conn,user);
                        request.setAttribute("errorMsg", "注册成功！");
                        RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
                        rd.forward(request, response);
                    }else {
                        request.setAttribute("errorMsg", "用户名已存在！");
                        RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
                        rd.forward(request, response);
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }

            }
        } else {
            request.setAttribute("errorMsg", "两次输入的密码不一致！");
            RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
            rd.forward(request, response);
        }
    }
}
