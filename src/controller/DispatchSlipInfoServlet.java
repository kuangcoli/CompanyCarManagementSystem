package controller;

import modle.Bean.DispatchSlip;
import modle.Bean.OrderNotification;
import modle.Service.DispatchSlipService;
import modle.Service.OrderService;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serial;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @className: DispatchSlipInfoServlet
 * @description: TODO 类描述
 * @author: 匡科利
 * @date: 2023/05/29 20:49
 * @Company: Copyright© [日期] by [作者或个人]
 **/

@WebServlet("/DispatchSlipInfo")
public class DispatchSlipInfoServlet extends HttpServlet {
    @Serial
    private static final long serialVersionUID = 14L;
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DispatchSlipService dispatcher = new DispatchSlipService();
        Map<String, Object> result = new HashMap<>();
        List<DispatchSlip> dispatchSlipList = dispatcher.getDispatchSilpList();
        result.put("code", 0);
        result.put("msg", "请求成功");
        result.put("count", dispatchSlipList.size());
        result.put("data", dispatchSlipList);
        JSONObject json = new JSONObject(result);
        String jsonStr = json.toString();
        response.setContentType("text/javascript;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.write(jsonStr);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
