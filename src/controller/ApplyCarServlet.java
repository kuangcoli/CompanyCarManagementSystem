package controller;

import modle.Bean.ApplyCarForm;
import modle.Service.ApplyCarService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serial;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@WebServlet("/ApplyCar")
public class ApplyCarServlet extends HttpServlet {
    @Serial
    private static final long serialVersionUID = 8L;
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取申请表数据 构造申请表实体并初始化 调用service方法执行插入
        ApplyCarService applyCarService = new ApplyCarService();
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String username = request.getParameter("username");
        int apply_type = Integer.parseInt(request.getParameter("apply_type"));
        System.out.println("申请类型："+apply_type);
        String remarks = request.getParameter("remarks");
        int car_type = Integer.parseInt(request.getParameter("car_type"));
        String start_date = request.getParameter("start_date");
        int num = Integer.parseInt(request.getParameter("person_num"));
        String start_location = request.getParameter("start_location");
        String destination = request.getParameter("destination");
        String apply_date =  now.format(formatter);
        ApplyCarForm applyCarForm = new ApplyCarForm();
        applyCarForm.setStart_date(start_date);
        applyCarForm.getApplicant().setUsername(username);
        applyCarForm.setApply_type(apply_type);
        applyCarForm.setRemarks(remarks);
        applyCarForm.setCar_type(car_type);
        applyCarForm.setPerson_num(num);
        applyCarForm.setApply_date(apply_date);
        applyCarForm.setStart_date(start_date);
        applyCarForm.setStart_location(start_location);
        applyCarForm.setDestination(destination);
        //添加用车申请时将用车申请的申请状态设置为1 （已提交状态）
        applyCarForm.setStatus(1);
        //调用service
        try {
            applyCarService.addApplyCar(applyCarForm);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


    }
}
