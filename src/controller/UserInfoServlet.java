package controller;

import com.mysql.cj.xdevapi.JsonString;
import modle.Bean.JDBCUtil;
import modle.Bean.User;
import modle.DAO.UserDao;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serial;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



@WebServlet("/UserInfo")
public class UserInfoServlet extends HttpServlet {
    @Serial
    private static final long serialVersionUID = 4L;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 设置响应的字符编码和数据类型
        response.setContentType("application/json; charset=utf-8");
        // 获取请求参数
        String username = request.getParameter("username");  // 示例中假设有一个名为param1的请求参数
        String roletype = request.getParameter("roletype");
        JDBCUtil db = new JDBCUtil();
        Connection conn = db.getConn();
        // 判断是否提供了参数
        if ((username == null || username.isEmpty())&&(roletype == null || roletype.isEmpty())) {  // 如果请求中没有提供参数，则获取所有用户数据
            UserDao userDao = new UserDao();  // 先实例化UserDao类
            // 将userList数据转换为json格式数据
            Map<String, Object> result = new HashMap<>();
            List<User> userList = userDao.getAllUsers(conn);  // 获取所有用户数据
            result.put("code", 0);
            result.put("msg", "请求成功");
            result.put("count", userList.size());
            result.put("data", userList);
            JSONObject json = new JSONObject(result);
            String jsonStr = json.toString();
            try (PrintWriter out = response.getWriter()) {
                out.write(jsonStr);
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if ((username != null || !username.isEmpty())&&(roletype == null || roletype.isEmpty())){  // 如果请求中提供了参数，则根据参数获取用户数据
            UserDao userDao = new UserDao();  // 先实例化UserDao类
            List<User> userList = userDao.getUsersByUsername(conn,username);  // 根据参数获取用户数据
            // 将userList数据转换为json格式数据
            Map<String, Object> result = new HashMap<>();
            result.put("code", 0);
            result.put("msg", "请求成功");
            result.put("count", userList.size());
            result.put("data", userList);
            JSONObject json = new JSONObject(result);
            String jsonStr = json.toString();

            try (PrintWriter out = response.getWriter()) {
                out.write(jsonStr);
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if ((username == null || username.isEmpty())&&(roletype != null || !roletype.isEmpty())) {
            UserDao userDao = new UserDao();  // 先实例化UserDao类
            List<User> userList = userDao.getUsersByroletype(conn,roletype);  // 根据参数获取用户数据
            // 将userList数据转换为json格式数据
            Map<String, Object> result = new HashMap<>();
            result.put("code", 0);
            result.put("msg", "请求成功");
            result.put("count", userList.size());
            result.put("data", userList);
            JSONObject json = new JSONObject(result);
            String jsonStr = json.toString();
            System.out.println(jsonStr);
            try (PrintWriter out = response.getWriter()) {
                out.write(jsonStr);
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            UserDao userDao = new UserDao();  // 先实例化UserDao类
            List<User> userList = userDao.getUsersByNameAndType(conn,username,roletype);  // 根据参数获取用户数据
            // 将userList数据转换为json格式数据
            Map<String, Object> result = new HashMap<>();
            result.put("code", 0);
            result.put("msg", "请求成功");
            result.put("count", userList.size());
            result.put("data", userList);
            JSONObject json = new JSONObject(result);
            String jsonStr = json.toString();

            try (PrintWriter out = response.getWriter()) {
                out.write(jsonStr);
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
