package modle.DAO;

import modle.Bean.ApplyCarRecord;
import modle.Bean.DispatchSlip;
import modle.Bean.OrderNotification;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * @className: OrderDao
 * @description 订单dao 连接数据库中与订单有关的内容
 * @author: 匡科利
 * @date: 2023/05/29 15:42
 * @Company: Copyright© [日期] by [作者或个人]
 **/
public class OrderDao {
    public OrderDao() {
    }

    //获取订单通知数据 通过用户id
    public List<OrderNotification> getOrderNotifications(Connection conn , int employeeID) {
        PreparedStatement pstmt = null;
        List<OrderNotification> orderNotificationList = null;
        try {
            orderNotificationList = new ArrayList<>();
            pstmt = conn.prepareStatement("SELECT *  FROM order_notification WHERE  customer = ? ");
            pstmt.setInt(1, employeeID);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                OrderNotification orderNotification = new OrderNotification();
                orderNotification.setStart_date(rs.getString("start_date"));
                orderNotification.setStart_location(rs.getString("start_location"));
                orderNotification.setDestination(rs.getString("destination"));
                orderNotification.setCar_type(rs.getString("cartype"));
                orderNotification.setCar_color(rs.getString("body_color"));
                orderNotification.setCar_lpn(rs.getString("LPN"));
                orderNotification.setCar_photo(rs.getString("car_photo"));
                orderNotification.setDriver_name(rs.getString("driver_name"));
                orderNotification.setDriver_photo(rs.getString("driver_photo"));
                orderNotification.setDriver_sex(rs.getString("sex"));
                orderNotification.setDriver_tel(rs.getString("tel"));
                orderNotification.setOrder_id(rs.getInt("order_id"));
                System.out.println(orderNotification.getOrder_id());
                orderNotificationList.add(orderNotification);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return orderNotificationList;
    }

    public List<DispatchSlip> getDispatchSilpList(Connection conn ) {
        //该方法通过获取申请人ID来返回当前申请车辆 即只获取申请状态为未结束状态 1 2 3
        PreparedStatement pstmt = null;
        List<DispatchSlip> dispatchSlipList = null;
        try {
            dispatchSlipList = new ArrayList<>();
            pstmt = conn.prepareStatement("SELECT *  FROM dispatchinfo ");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                DispatchSlip dispatchSlip = new DispatchSlip();
                dispatchSlip.setEmployeeID(rs.getInt("employee_id"));
                dispatchSlip.setReal_name(rs.getString("real_name"));
                dispatchSlip.setTel(rs.getString("tel"));
                dispatchSlip.setStart_date(rs.getString("start_date"));
                dispatchSlip.setPerson_num(rs.getInt("num"));
                dispatchSlip.setCarType(rs.getString("cartype"));
                dispatchSlip.setDestination(rs.getString("destination"));
                dispatchSlip.setApplytype(rs.getString("type"));
                dispatchSlip.setRemarks(rs.getString("remarks"));
                dispatchSlip.setApplySlipID(rs.getInt("applyvehicle_id"));
                dispatchSlip.setSex(rs.getString("sex"));
                dispatchSlip.setDept(rs.getString("deptname"));
                dispatchSlip.setStart_location(rs.getString("start_location"));
                dispatchSlip.setPhoto_url(rs.getString("photo_url"));
                dispatchSlipList.add(dispatchSlip);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return dispatchSlipList;
    }
}
