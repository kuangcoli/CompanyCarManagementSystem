package modle.Service;

import modle.Bean.ApplyCarRecord;
import modle.Bean.JDBCUtil;
import modle.Bean.OrderNotification;
import modle.DAO.ApplyCarDao;
import modle.DAO.OrderDao;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @className: OrderService
 * @description: 订单业务类
 * @author: 匡科利
 * @date: 2023/05/29 15:58
 * @Company: Copyright© [日期] by [作者或个人]
 **/
public class OrderService {
    public OrderService() {
    }

    public List<OrderNotification> getOrderNotifications(int employeeID){
        OrderDao orderDao  =new OrderDao();
        JDBCUtil db = new JDBCUtil();
        Connection conn = db.getConn();
        List<OrderNotification> orderNotifications = orderDao.getOrderNotifications(conn,employeeID);
        return orderNotifications;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ApplyCarService applyCarService = new ApplyCarService();
        int employeeID = Integer.parseInt(request.getParameter("employeeID"));
        Map<String, Object> result = new HashMap<>();
        List<ApplyCarRecord> currentApplyCarRecords= applyCarService.getCurrentApplyCarRecords(employeeID);
        result.put("code", 0);
        result.put("msg", "请求成功");
        result.put("count", currentApplyCarRecords.size());
        result.put("data", currentApplyCarRecords);
        JSONObject json = new JSONObject(result);
        String jsonStr = json.toString();
        response.setContentType("text/javascript;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.write(jsonStr);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
