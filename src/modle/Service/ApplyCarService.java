package modle.Service;

import modle.Bean.ApplyCarForm;
import modle.Bean.ApplyCarRecord;
import modle.Bean.JDBCUtil;
import modle.DAO.ApplyCarDao;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.util.List;

public class ApplyCarService {
    public ApplyCarService() {
    }

    public void addApplyCar(@NotNull ApplyCarForm applyCarForm) throws Exception {
        ApplyCarDao applyCarDao = new ApplyCarDao();
        JDBCUtil db = new JDBCUtil();
        Connection conn = db.getConn();
        //初始化申请员工信息
        applyCarForm.setApplicant(applyCarDao.getEmployeeInfo(conn,applyCarForm.getApplicant().getUsername()));
        //插入用车申请表
        applyCarDao.addApplyCar(conn,applyCarForm);
        //插入完关闭数据库连接
        conn.close();
    }

    public List<ApplyCarRecord> getCurrentApplyCarRecords(int employeeID){
        ApplyCarDao applyCarDao = new ApplyCarDao();
        JDBCUtil db = new JDBCUtil();
        Connection conn = db.getConn();
        List<ApplyCarRecord> applyCarRecords = applyCarDao.getcurrentApplyCarRecords(conn,employeeID);
        return applyCarRecords;
    }


    public List<ApplyCarRecord> getAllApplyCarRecords(int employeeID){
        ApplyCarDao applyCarDao = new ApplyCarDao();
        JDBCUtil db = new JDBCUtil();
        Connection conn = db.getConn();
        return applyCarDao.getAllApplyCarRecords(conn,employeeID);
    }

    public void deletApplyCar( ApplyCarForm applyCarForm) throws Exception {
        ApplyCarDao applyCarDao = new ApplyCarDao();
        JDBCUtil db = new JDBCUtil();
        Connection conn = db.getConn();
        applyCarDao.deleteApplyCar(conn,applyCarForm);
        conn.close();

    }

}
