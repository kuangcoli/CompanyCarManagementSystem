package modle.Service;

import modle.Bean.DispatchSlip;
import modle.Bean.JDBCUtil;
import modle.Bean.OrderNotification;
import modle.DAO.OrderDao;

import java.sql.Connection;
import java.util.List;

/**
 * @className: DispatchSlipService
 * @description: TODO 类描述
 * @author: 匡科利
 * @date: 2023/05/29 20:45
 * @Company: Copyright© [日期] by [作者或个人]
 **/
public class DispatchSlipService {
    public DispatchSlipService() {
    }

    public List<DispatchSlip> getDispatchSilpList( ) {
        OrderDao orderDao  =new OrderDao();
        JDBCUtil db = new JDBCUtil();
        Connection conn = db.getConn();
        return orderDao.getDispatchSilpList(conn);
    }
}
