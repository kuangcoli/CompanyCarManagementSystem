package modle.Bean;

public class Driver extends User{
    private String name;
    private String tel;
    private String dept;
    private String sex;
    private String age;
    private String address;
    private String birthday;
    private String email;
    private  String photo_url;
    private String onboard_date;
    private String license_level;

    public Driver() {

    }

    public Driver(String username, String password, String roletype) {
        super(username, password, roletype);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getTel() {
        return tel;
    }

    @Override
    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public String getOnboard_date() {
        return onboard_date;
    }

    public void setOnboard_date(String onboard_date) {
        this.onboard_date = onboard_date;
    }

    public String getLicense_level() {
        return license_level;
    }

    public void setLicense_level(String license_level) {
        this.license_level = license_level;
    }
}
