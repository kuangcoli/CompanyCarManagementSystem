package modle.Bean;

/**
 * @className: DispatchSlip
 * @description: 部管已审核通过的待调度车辆申请 即待调度单
 * @author: 匡科利
 * @date: 2023/05/29 20:08
 * @Company: Copyright© [日期] by [作者或个人]
 **/
public class DispatchSlip {
    public DispatchSlip() {
    }
    int applySlipID;

    String start_location;

    String photo_url;

    String carType;

    String dept;


    String sex;


    String applytype;

    int employeeID;

    String real_name;

    String tel;

    String start_date;
    String destination;

    String remarks;

    int  person_num;


    public int getEmployeeID() {
        return employeeID;
    }

    public String getStart_location() {
        return start_location;
    }

    public void setStart_location(String start_location) {
        this.start_location = start_location;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public String getReal_name() {
        return real_name;
    }

    public void setReal_name(String real_name) {
        this.real_name = real_name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getPerson_num() {
        return person_num;
    }

    public void setPerson_num(int person_num) {
        this.person_num = person_num;
    }



    public int getApplySlipID() {
        return applySlipID;
    }

    public void setApplySlipID(int applySlipID) {
        this.applySlipID = applySlipID;
    }



    public String getApplytype() {
        return applytype;
    }

    public void setApplytype(String applytype) {
        this.applytype = applytype;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }
}
