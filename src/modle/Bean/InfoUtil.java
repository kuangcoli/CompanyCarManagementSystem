package modle.Bean;
import java.time.LocalDate;
import java.time.Period;
public class InfoUtil {
    public static int calculateAge(String birthday) {
        LocalDate birthDate = LocalDate.parse(birthday);
        LocalDate currentDate = LocalDate.now();
        Period period = Period.between(birthDate, currentDate);
        return period.getYears();
    }
}
