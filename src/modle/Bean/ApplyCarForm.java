package modle.Bean;

public class ApplyCarForm {
    Employee applicant = new Employee(); //申请人
    int car_type ; //申请车型
    int person_num ;//随行人数
    String start_date , apply_date; //申请时间 用车时间
    String start_location,destination;//起点，终点
    int apply_type;//申请事由类型
    String remarks;//备注 即用车事由的详细说明
    String dept_opinion;//部管意见
    String vm_opinion;//车管意见
    int status;//申请状态

    public ApplyCarForm() {
    }

    public Employee getApplicant() {
        return applicant;
    }

    public void setApplicant(Employee applicant) {
        this.applicant = applicant;
    }

    public int getCar_type() {
        return car_type;
    }

    public void setCar_type(int car_type) {
        this.car_type = car_type;
    }

    public int getPerson_num() {
        return person_num;
    }

    public void setPerson_num(int person_num) {
        this.person_num = person_num;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getApply_date() {
        return apply_date;
    }

    public void setApply_date(String apply_date) {
        this.apply_date = apply_date;
    }

    public String getStart_location() {
        return start_location;
    }

    public void setStart_location(String start_location) {
        this.start_location = start_location;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getApply_type() {
        return apply_type;
    }

    public void setApply_type(int apply_type) {
        this.apply_type = apply_type;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDept_opinion() {
        return dept_opinion;
    }

    public void setDept_opinion(String dept_opinion) {
        this.dept_opinion = dept_opinion;
    }

    public String getVm_opinion() {
        return vm_opinion;
    }

    public void setVm_opinion(String vm_opinion) {
        this.vm_opinion = vm_opinion;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
