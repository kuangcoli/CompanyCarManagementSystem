package modle.Bean;

public class User {
    private String username,password,roletype,regist_date,real_name,tel;

    public String getRegist_date() {
        return regist_date;
    }



    public void setRegist_date(String regist_date) {
        this.regist_date = regist_date;
    }

    public String getReal_name() {
        return real_name;
    }

    public void setReal_name(String real_name) {
        this.real_name = real_name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public User(String username, String password, String roletype) {
        this.username = username;
        this.password = password;
        this.roletype = roletype;
    }

    public String getUsername() {
        return username;
    }

    public User() {
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoletype() {
        return roletype;
    }

    public void setRoletype(String roletype) {
        this.roletype = roletype;
    }
}
