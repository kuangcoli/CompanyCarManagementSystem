package modle.Bean;

/**
 * @className: OrderNotification
 * @description: 用于封装订单的详细信息 包括司机的照片地址 车辆的照片地址等详细信息 数据来源于订单通知视图
 * @author: 匡科利
 * @date: 2023/05/29 15:25
 * @Company: Copyright© [日期] by [作者或个人]
 **/
public class OrderNotification {

    int order_id ;

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    String start_date;//发车时间
    String start_location;//发车地点
    String destination;//目的地
    String driver_name;//司机姓名
    String driver_tel;//司机联系电话
    String driver_sex;//司机性别
    String driver_photo;//司机照片
    String car_lpn;//车辆车牌号
    String car_color;//车身颜色
    String car_photo;//车辆照片
    String car_type;//车辆类型

    int status = 0;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public OrderNotification() {


    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getStart_location() {
        return start_location;
    }

    public void setStart_location(String start_location) {
        this.start_location = start_location;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getDriver_tel() {
        return driver_tel;
    }

    public void setDriver_tel(String driver_tel) {
        this.driver_tel = driver_tel;
    }

    public String getDriver_sex() {
        return driver_sex;
    }

    public void setDriver_sex(String driver_sex) {
        this.driver_sex = driver_sex;
    }

    public String getDriver_photo() {
        return driver_photo;
    }

    public void setDriver_photo(String driver_photo) {
        this.driver_photo = driver_photo;
    }

    public String getCar_lpn() {
        return car_lpn;
    }

    public void setCar_lpn(String car_lpn) {
        this.car_lpn = car_lpn;
    }

    public String getCar_color() {
        return car_color;
    }

    public void setCar_color(String car_color) {
        this.car_color = car_color;
    }

    public String getCar_photo() {
        return car_photo;
    }

    public void setCar_photo(String car_photo) {
        this.car_photo = car_photo;
    }

    public String getCar_type() {
        return car_type;
    }

    public void setCar_type(String car_type) {
        this.car_type = car_type;
    }
}
