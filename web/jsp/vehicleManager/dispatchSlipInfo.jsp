<%--
  Created by IntelliJ IDEA.
  User: Tsuki
  Date: 2023-05-29
  Time: 22:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>待调度订单信息表</title>

    <style>
        td {
            text-align: center;
        }
        #tablehead{
            text-align: center;
        }
    </style>
</head>
<body>
<h1 id="tablehead">待调度订单信息表</h1>
<table class="layui-table" lay-size="lg">
    <tbody>
    <tr>
        <td style="width: 100px">申请编号</td>
        <td style="width: 100px" id="employeeID"></td>
        <td style="width: 100px">乘客姓名</td>
        <td style="width: 100px" id="real_name"></td>
        <td style="width: 100px">联系电话</td>
        <td style="width: 100px" id="tel"></td>
        <td style="width: 100px" rowspan="2" > <img id="photo_url"> </td>
    </tr>
    <tr>
        <td style="width: 100px">所在部门</td>
        <td style="width: 100px" id="dept"></td>
        <td style="width: 100px">性别</td>
        <td style="width: 100px" id="sex"></td>
        <td style="width: 100px">申请事由</td>
        <td style="width: 100px" id="apply_type"></td>

    </tr>
    <tr>
        <td>申请车型</td>
        <td id="car_type"></td>
        <td>随行人数</td>
        <td id="person_num"></td>
        <td>用车时间</td>
        <td id="start_date" colspan="2"></td>
    </tr>
    <tr>
        <td>上车地点</td>
        <td colspan="6" id="start_location"></td>
    </tr>
    <tr>
        <td>目的地</td>
        <td colspan="6" id="destination"></td>

    </tr>
    <tr>
        <td>申请备注</td>
        <td colspan="6" id="remarks"></td>
    </tr>
    <tr>
        <td rowspan="2">备注</td>
        <td colspan="6" >
            1、公司车辆外出需办理用车申请表，经批准后方可外出
            2、公司借给申请人用车，申请借车人为第一责任人，不允许转借给其他人使用，若要借给他人使用。责任人要承担全部责任。
        </td>
    </tr>
    <tr></tr>
    </tbody>
</table>
<script>
    $(function () {
        // 读取全局变量中的数据对象
        var data = window.parent.applycarInfo;
        var img = document.getElementById("photo_url");
        img.src = data.rowdata.photo_url;
        $('#tel').text(data.rowdata.tel);
        $('#dept').text(data.rowdata.dept);
        $('#sex').text(data.rowdata.sex);
        $('#employeeID').text(data.rowdata.employeeID);
        $('#real_name').text(data.rowdata.real_name);
        $('#apply_type').text(data.rowdata.applytype);
        $('#car_type').text(data.rowdata.carType);
        $('#person_num').text(data.rowdata.person_num);
        $('#apply_date').text(data.rowdata.apply_date);
        $('#start_date').text(data.rowdata.start_date);
        $('#status').text(data.rowdata.status);
        $('#start_location').text(data.rowdata.start_location);
        $('#destination').text(data.rowdata.destination);
        $('#remarks').text(data.rowdata.remarks);
        window.applycarInfo = null;
    });
</script>
</body>
</html>

