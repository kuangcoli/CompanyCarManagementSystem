<%--
  Created by IntelliJ IDEA.
  User: Tsuki
  Date: 2023-05-29
  Time: 19:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>当前申请</title>
</head>
<body>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main">
        <script type="text/html" id="toolbar">
            <div class="layui-btn-container">
            </div>
        </script>
        <table class="layui-hide" id="currentApplyRecordsTable" lay-filter="currentTableFilter"></table>
        <script type="text/html" id="currentTableBar">
            <a class="layui-btn layui-btn-primary layui-btn-xs data-count-view" lay-event="view">查看</a>
            <a class="layui-btn layui-btn-normal layui-btn-xs data-count-view" lay-event="view">派单</a>
            <a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete" lay-event="delete">拒绝</a>
        </script>

    </div>
</div>

<script>

    layui.use(['form', 'table', 'miniPage', 'element'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            miniPage = layui.miniPage;
        $.ajax({
            url: '../../DispatchSlipInfo',
            method: 'POST',
            dataType: 'json',
            success: function(res){
                // 渲染表格
                table.render({
                    elem: '#currentApplyRecordsTable',
                    data: res.data,
                    toolbar: '#toolbar',
                    //默认工具条 筛选  导出  打印
                    defaultToolbar: ['filter', 'exports', 'print', {
                        title: '提示',
                        layEvent: 'LAYTABLE_TIPS',
                        icon: 'layui-icon-tips'
                    }],
                    cols: [[
                        {field: 'applySlipID', width: 120, title: '申请编号', sort: true},
                        {field: 'real_name', width: 110, title: '申请人', sort: true},
                        {field: 'tel', width: 110, title: '联系电话', sort: true, hide: true},
                        {field: 'dept', width: 110, title: '所在部门', sort: true, hide: true},
                        {field: 'photo_url', width: 110, title: '个人照片', sort: true, hide: true},
                        {field: 'applytype', width: 120, title: '申请事由'},
                        {field: 'carType', width: 100, title: '申请车型'},
                        {field: 'start_date', width: 180, title: '用车时间', sort: true},
                        {field: 'start_location', width: 150, title: '出车地点', sort: true,hide: true},
                        {field: 'destination', width: 150, title: '目的地', sort: true},
                        {field: 'person_num', width: 120, title: '人数', sort: true },
                        {field: 'remarks', width: 120, title: '备注', sort: true ,hide:true},
                        {
                            title: '操' +
                                '作', minWidth: 150, toolbar: '#currentTableBar', align: "center"
                        }
                    ]],
                    //分页数
                    limits: [10, 15, 20, 25, 50, 100],
                    limit: 10,
                    page: true,
                    skin: 'line'
                });
            },
            error: function(err){
                console.log(err);
            }
        });

        //监听表格复选框选择
        table.on('checkbox(currentTableFilter)', function (obj) {
            console.log(obj)
        });

        table.on('tool(currentTableFilter)', function (obj) {
            var rowdata = obj.data;
            if (obj.event === 'view') {
                window.applycarInfo ={
                    rowdata : rowdata
                };
                var content = miniPage.getHrefContent('dispatchSlipInfo.jsp' );
                var openWH = miniPage.getOpenWidthHeight();
                var index = layer.open({
                    title: '调度信息',
                    type: 1,
                    shade: 0,
                    maxmin: true,
                    shadeClose: true,
                    area: [openWH[0] + 'px', openWH[1] + 'px'],
                    offset: [openWH[2] + 'px', openWH[3] + 'px'],
                    content: content,
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
                return false;
            } else if (obj.event === 'delete') {
                layer.confirm('确认撤销', function (index) {
                    $.ajax({
                        url: "../../DeleteApplyCar",
                        type: "POST",
                        dataType: 'json',
                        data: {
                            applyStatus : 0,
                            employeeID : employeeID,
                            apply_date : rowdata.apply_date,
                            start_date : rowdata.start_date,
                            start_location : rowdata.start_location,
                            destination :rowdata.destination,
                        },
                        success: function (res) {
                            table.reload('currentApplyRecordsTable',{
                                data:res.data,
                            });
                        }
                    });
                    layer.close(index);
                });
            }
        });

    });

</script>
</body>
</html>
