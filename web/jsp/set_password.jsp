<%@ page import="modle.Bean.User" %><%--
  Created by IntelliJ IDEA.
  User: Tsuki
  Date: 2023-04-30
  Time: 15:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="../lib/layer/skin/layer.css">
    <style>
        .layui-form-item .layui-input-company {
            width: auto;
            padding-right: 10px;
            line-height: 38px;
        }
    </style>

</head>
<body>

<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main">
        <form class="layui-form layuimini-form" action="../../ModifyPassword"   method="post">
            <div class="layui-form-item">
                <label class="layui-form-label required">旧的密码</label>
                <div class="layui-input-block">
                    <input type="password" name="old_password" id="old_password" lay-verify="required" lay-reqtext="旧的密码不能为空" placeholder="请输入旧的密码" value="" class="layui-input">
                    <tip>填写自己账号的旧的密码。</tip>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label required">新的密码</label>
                <div class="layui-input-block">
                    <input type="password" name="new_password" lay-verify="required" lay-reqtext="新的密码不能为空" placeholder="请输入新的密码" value="" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label required">确认新密码</label>
                <div class="layui-input-block">
                    <input type="password" name="again_password" lay-verify="required" lay-reqtext="新的密码不能为空" placeholder="请输入新的密码" value="" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn layui-btn-normal"  lay-submit lay-filter="saveBtn" >确认保存</button>
                </div>
            </div>
        </form>
    </div>
</div>


<script>
    layui.use(['form', 'miniPage'], function () {
        var form = layui.form,
            layer = layui.layer,
            miniPage = layui.miniPage;
        /**
         * 初始化表单，要加上，不然刷新部分组件可能会不加载
         */
        form.render();
        //监听提交
        form.on('submit(saveBtn)', function (data) {
            // 获取当前用户密码
            var password = "<%= ((User)session.getAttribute("user")).getPassword() %>";
            // 获取用户输入的旧密码
            var oldPassword = data.field.old_password;
            // 判断用户输入的旧密码是否正确
            if (oldPassword !== password) {
                layer.msg("原密码验证错误！");
                return false;
            }
            return true;
        });
    });
</script>
</body>
</html>
