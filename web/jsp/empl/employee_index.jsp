<%@ page import="modle.Bean.Employee" %><%--
  Created by IntelliJ IDEA.
  User: Tsuki
  Date: 2023-04-24
  Time: 22:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<html>

<head>
    <meta charset="utf-8">
    <%--    指定当前页面在不同浏览器下的渲染方式  指定webkit内核--%>
    <meta name="renderer" content="webkit">
    <%--    IE使用最新版本的渲染引擎进行页面渲染，并尽量兼容Chrome浏览器--%>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <%--    <meta http-equiv="Access-Control-Allow-Origin" content="*">--%>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <%--    设置网页标题的图标--%>
    <link rel="icon" href="../../img/favicon.ico">
<%--    <link rel="stylesheet" href="../lib/layui-v2.8.1/layui/css/layui.css" media="all">--%>
    <link rel="stylesheet" href="../../lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="../../lib/font-awesome-4.7.0/css/font-awesome.min.css" media="all">
    <link rel="stylesheet" href="../../css/layuimini.css?v=2.0.1" media="all">
    <link rel="stylesheet" href="../../css/themes/default.css" media="all">
    <script src="../../lib/jquery-3.4.1/jquery-3.4.1.min.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../../css/public.css" media="all">
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <title>普通用户后台</title>
    <style id="layuimini-bg-color">
    </style>
    <%
        session.removeAttribute("errorMsg");
    %>
    <%
        Employee employee = (Employee) session.getAttribute("employee");
    %>
</head>
<body class="layui-layout-body layuimini-all">
<%--管理系统布局--%>
    <div class="layui-layout layui-layout-admin">
<%--头部导航栏--%>
        <div class="layui-header header">
<%--头部导航栏内logo--%>
            <div class="layui-logo layuimini-logo layuimini-back-home"></div>

            <div class="layuimini-header-content">
                <a>
                    <div class="layuimini-tool"><i title="展开" class="fa fa-outdent" data-side-fold="1"></i></div>
                </a>

                <!--电脑端头部菜单-->
                <ul class="layui-nav layui-layout-left layuimini-header-menu layuimini-menu-header-pc layuimini-pc-show">
                </ul>
                <%--头部导航栏右侧工具--%>
                <ul class="layui-nav layui-layout-right">
<%--                    刷新--%>

                    <li class="layui-nav-item" lay-unselect>
                        <a href="javascript:;" data-refresh="刷新"><i class="fa fa-refresh"></i></a>
                    </li>
<%--    清空缓存--%>
                    <li class="layui-nav-item" lay-unselect>
                        <a href="javascript:;" data-clear="清理" class="layuimini-clear"><i class="fa fa-trash-o"></i></a>
                    </li>
<%--                    是否展开为全屏--%>
                    <li class="layui-nav-item mobile layui-hide-xs" lay-unselect>
                        <a href="javascript:;" data-check-screen="full"><i class="fa fa-arrows-alt"></i></a>
                    </li>
<%--                    个人中心 admin--%>
                    <li class="layui-nav-item layuimini-setting">
                        <a href="javascript:;">
                            <img src="<%=employee.getPhoto_url()%>" class="layui-nav-img">
                            个人中心</a>
                        <dl class="layui-nav-child">
                            <dd>
                                <a href="javascript:
                                ;" layuimini-content-href="../../jsp/empl/empl_setting.jsp" data-title="基本资料"
                                   data-icon="fa fa-gears">基本资料<span class="layui-badge-dot"></span></a>
                            </dd>
                            <dd>
                                <a href="javascript:;" layuimini-content-href="../../jsp/set_password.jsp"
                                   data-title="修改密码" data-icon="fa fa-gears">修改密码</a>
                            </dd>
                            <dd>
                                <hr>
                            </dd>
                            <dd>
                                <a href="javascript:;" class="login-out">退出登录</a>
                            </dd>
                        </dl>
                    </li>
<%--               配色方案选择 及文档地址     --%>
                    <li class="layui-nav-item layuimini-select-bgcolor" lay-unselect>
                        <a href="javascript:;" data-bgcolor="配色方案"><i class="fa fa-ellipsis-v"></i></a>
                    </li>
                </ul>
            </div>
        </div>

        <!--无限极左侧菜单-->
        <div class="layui-side layui-bg-black layuimini-menu-left">

        </div>

        <!--初始化加载层-->
        <div class="layuimini-loader">
            <div class="layuimini-loader-inner"></div>
        </div>
    <div class="layui-body">

        <div class="layuimini-content-page">
        </div>

    </div>

    </div>
<%
    String errorMsg = (String) request.getAttribute("errorMsg");
    if (errorMsg != null && !"".equals(errorMsg)) {
%>
<script>
    $(function () {
        layer.msg("<%= errorMsg %>");
    });
</script>
<%
    }
%>
<script src="../../lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script src="../../js/lay-config.js?v=2.0.0" charset="utf-8"></script>
<script>
    layui.use(['jquery', 'layer', 'miniAdmin', 'miniTongji'], function () {
        var $ = layui.jquery,
            layer = layui.layer,
            miniAdmin = layui.miniAdmin,
            miniTongji = layui.miniTongji;

        var options = {
            url:"",
            iniUrl: "../../api/empl_init.json",    // 初始化接口
            clearUrl: "../../api/clear.json", // 缓存清理接口
            renderPageVersion: true,    // 初始化页面是否加版本号
            bgColorDefault: false,      // 主题默认配置
            multiModule: true,          // 是否开启多模块
            menuChildOpen: false,       // 是否默认展开菜单
            loadingTime: 0,             // 初始化加载时间
            pageAnim: true,             // 切换菜单动画
        };
        miniAdmin.render(options);

        // 百度统计代码，只统计指定域名
        miniTongji.render({
            specific: true,
            domains: [
                '99php.cn',
                'layuimini.99php.cn',
                'layuimini-onepage.99php.cn',
            ],
        });

        $('.login-out').on("click", function () {
            layer.msg('退出登录成功', function () {
                window.location = '../../index.jsp';
            });
        });
    });
</script>
</body>
</html>
