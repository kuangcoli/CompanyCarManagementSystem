<%--
  Created by IntelliJ IDEA.
  User: Tsuki
  Date: 2023-05-04
  Time: 22:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>用户注册</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content ="telephone=no">
    <link rel="stylesheet" href="../../lib/layui-v2.8.1/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="./css/login.css">
    <link rel="stylesheet" href="./lib/layer/skin/layer.css">
    <script src="./lib/jquery-3.4.1/jquery-3.4.1.min.js" charset="utf-8"></script>
    <script src="./lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
    <script src="./lib/jq-module/jquery.particleground.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="./lib/layer/layer.js"></script>
</head>
<body>

</body>
</html>
