<%@ page import="modle.Bean.Employee" %><%--
  Created by IntelliJ IDEA.
  User: Tsuki
  Date: 2023-04-28
  Time: 23:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>个人基本资料</title>
</head>
<body>
<%
    Employee employee = (Employee) session.getAttribute("employee");
%>
<div class="layuimini-container">
    <fieldset class="layui-elem-field">
        <legend>个人信息</legend>
        <div class="layui-field-box">
            <form class="layui-form" action="">
                <div class="layui-form-item">
                    <label class="layui-form-label">用户ID：</label>
                    <div class="layui-input-inline">
                        <input type="text" name="username" value="<%= employee.getUsername()%>" class="layui-input" readonly>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">姓名：</label>
                    <div class="layui-input-inline">
                        <input type="text" name="username" value="<%=employee.getName()%>" class="layui-input" readonly>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">性别：</label>
                    <div class="layui-input-inline">
                        <input type="text" name="sex" value="<%=employee.getSex()%>" class="layui-input" readonly>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">个人照片：</label>
                    <div class="layui-input-inline">
                        <img src="<%=employee.getPhoto_url()%>" alt="个人照片" width="100px">
                        <button type="button" class="layui-btn layui-btn-primary layui-btn-sm" style="margin-left: 5px;">上传照片</button>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">所属部门：</label>
                    <div class="layui-input-inline">
                        <input type="text" name="department" value="<%=employee.getDept()%>" class="layui-input" readonly>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">地址：</label>
                    <div class="layui-input-inline">
                        <input type="text" name="address" value="<%=employee.getAddress()%>" class="layui-input" readonly>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">年龄：</label>
                    <div class="layui-input-inline">
                        <input type="text" name="age" value="<%=employee.getAge()%>" class="layui-input" readonly>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">联系电话：</label>
                    <div class="layui-input-inline">
                        <input type="text" name="phone" value="<%=employee.getTel()%>" class="layui-input" readonly>
                    </div>
                </div>


                <div class="layui-form-item layui-hide">
                    <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
                </div>
            </form>
            <button id="editBtn" class="layui-btn layui-btn-sm"><i class="layui-icon layui-icon-edit"></i>编辑</button>
            <button id="saveBtn" class="layui-btn layui-btn-sm layui-hide"><i class="layui-icon layui-icon-ok"></i>保存</button>
        </div>
    </fieldset>
</div>

<script>

    // 初始化 layui
    layui.use(['form', 'upload'], function(){
        var form = layui.form,
            upload = layui.upload;

        // 文件上传
        upload.render({
            elem: '.layui-btn-primary',
            url: '/upload/',
            done: function(res){
                console.log(res);
            }
        });

        // 编辑按钮事件
        var editBtn = document.getElementById('editBtn'),
            saveBtn = document.getElementById('saveBtn');
        editBtn.onclick = function() {
            // 可编辑状态
            let inputs = document.getElementsByTagName('input');
            for(let i = 0; i < inputs.length; i++) {
                inputs[i].readOnly = false;
            }
            form.render();
            editBtn.classList.add('layui-hide');
            saveBtn.classList.remove('layui-hide');
        };

        // 保存按钮事件
        saveBtn.onclick = function() {
            // 不可编辑状态
            let inputs = document.getElementsByTagName('input');
            for(let i = 0; i < inputs.length; i++) {
                inputs[i].readOnly = true;
            }
            form.render();

            saveBtn.classList.add('layui-hide');
            editBtn.classList.remove('layui-hide');

            // TODO: 更新数据库中的信息，可以使用 ajax 请求将修改后的数据提交到后端进行处理
            $.ajax({
                type: 'POST',
                url: '/update',
                data: JSON.stringify(data),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function(res) {
                    console.log(res);
                },
                error: function(xhr, status, error) {
                    console.error(error);
                }
            });
        };
    });
</script>

</body>
</html>