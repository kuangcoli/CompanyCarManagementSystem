<%@ page import="modle.Bean.Employee" %><%--
  Created by IntelliJ IDEA.
  User: Tsuki
  Date: 2023-05-24
  Time: 23:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>所有申请</title>
</head>
<body>
<%
    Employee employee = (Employee) session.getAttribute("employee");
%>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main">
        <table class="layui-hide" id="allApplyRecordsTable" lay-filter="currentTableFilter"></table>
        <script type="text/html" id="currentTableBar">
            <a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit" lay-event="view">查看</a>
            <a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete" lay-event="delete">删除</a>
        </script>

    </div>
</div>

<script>

    layui.use(['form', 'table', 'miniPage', 'element'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            miniPage = layui.miniPage;
        var employeeID = <%= employee.getEmployeeID()%>
        var tel = <%= employee.getTel()%>;
        var dept = "<%= employee.getDept()%>";
        var real_name = " <%= employee.getName()%>";
            $.ajax({
                url: '../../AllApply',
                method: 'POST',
                dataType: 'json',
                data: {
                    employeeID :employeeID,
                },
                success: function(res){
                    // 渲染表格
                    table.render({
                        elem: '#allApplyRecordsTable',
                        data: res.data,
                        //默认工具条 筛选  导出  打印
                        defaultToolbar: ['filter', 'exports', 'print', {
                            title: '提示',
                            layEvent: 'LAYTABLE_TIPS',
                            icon: 'layui-icon-tips'
                        }],
                        cols: [[
                            {type: "checkbox", width: 100},
                            {field: 'apply_date', width: 180, title: '申请时间', sort: true},
                            {field: 'apply_type', width: 120, title: '申请事由'},
                            {field: 'car_type', width: 80, title: '车型'},
                            {field: 'apply_date', width: 180, title: '用车时间', sort: true},
                            {field: 'destination', width: 120, title: '目的地', sort: true},
                            {field: 'status', width: 150, title: '状态', sort: true},
                            {
                                title: '操' +
                                    '作', minWidth: 140, toolbar: '#currentTableBar', align: "center"
                            }
                        ]],
                        //分页数
                        limits: [10, 15, 20, 25, 50, 100],
                        limit: 10,
                        page: true,
                        skin: 'line'
                    });
                },
                error: function(err){
                    console.log(err);
                }
            });

        //监听表格复选框选择
        table.on('checkbox(currentTableFilter)', function (obj) {
            console.log(obj)
        });

        table.on('tool(currentTableFilter)', function (obj) {
            var rowdata = obj.data;
            if (obj.event === 'view') {
                window.applycarInfo ={
                    rowdata : rowdata,
                    tel: tel,
                    dept:dept,
                    real_name :real_name
                };
                var content = miniPage.getHrefContent('applyCarFormInfo.jsp' );
                var openWH = miniPage.getOpenWidthHeight();
                var index = layer.open({
                    title: '申请信息',
                    type: 1,
                    shade: 0,
                    maxmin: true,
                    shadeClose: true,
                    area: [openWH[0] + 'px', openWH[1] + 'px'],
                    offset: [openWH[2] + 'px', openWH[3] + 'px'],
                    content: content,
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
                return false;
            } else if (obj.event === 'delete') {
                layer.confirm('确认删除', function (index) {
                    $.ajax({
                        url: "../../DeleteApplyCar",
                        type: "POST",
                        dataType: 'json',
                        data: {
                            applyStatus : 1,
                            employeeID : employeeID,
                            apply_date : data.apply_date,
                            start_date : data.start_date,
                            start_location : data.start_location,
                            destination :data.destination,
                        },
                        success: function (res) {
                            table.reload('allApplyRecordsTable',{
                                data:res.data,
                            });
                        }
                    });
                    layer.close(index);
                });
            }
        });

    });

</script>
</body>
</html>
