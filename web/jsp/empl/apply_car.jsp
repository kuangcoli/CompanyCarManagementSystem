<%@ page import="modle.Bean.Employee" %><%--
  Created by IntelliJ IDEA.
  User: Tsuki
  Date: 2023-05-01
  Time: 10:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="../../js/lay-module/step-lay/step.css" media="all">
</head>
<body>
<%
    Employee employee = (Employee) session.getAttribute("employee");
%>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main">
        <div class="layui-fluid">
            <div class="layui-card">
                <div class="layui-card-body" style="padding-top: 40px;">
                    <div class="layui-carousel" id="stepForm" lay-filter="stepForm" style="margin: 0 auto;">
                        <div carousel-item>
                            <div>
                                <form class="layui-form" style="margin: 0 auto;max-width: 460px;padding-top: 40px;">
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">申请人：</label>
                                        <div class="layui-input-block">
                                            <input type="text" id="real_name" value="<%=employee.getName()%>"
                                                   class="layui-input" readonly>
                                        </div>
                                    </div>

                                    <div class="layui-form-item">
                                        <label class="layui-form-label">员工号：</label>
                                        <div class="layui-input-block">
                                            <input type="text" id="username" value="<%=employee.getUsername()%>"
                                                   class="layui-input" readonly>
                                        </div>
                                    </div>

                                    <div class="layui-form-item">
                                        <label class="layui-form-label">所属部门：</label>
                                        <div class="layui-input-block">
                                            <input type="text" id="dept" value="<%=employee.getDept()%>"
                                                   class="layui-input" readonly>
                                        </div>
                                    </div>


                                    <div class="layui-form-item">
                                        <label class="layui-form-label required">联系电话：</label>
                                        <div class="layui-input-block">
                                            <input type="tel" id="tel" name="phone" value="<%=employee.getTel()%>"
                                                   placeholder="请填写在使用的电话号码" lay-verify="required|phone"
                                                   autocomplete="off" class="layui-input" readonly>
                                        </div>
                                    </div>

                                    <div class="layui-form-item">
                                        <label class="layui-form-label required">申请事由：</label>
                                        <div class="layui-col-md9">
                                            <select name="apply_type" id="apply_type" required lay-verify="required">
                                                <option disabled hidden >选择用车事由</option>
                                                <option value="1">差旅出行</option>
                                                <option value="2">物资配送</option>
                                                <option value="3">采购及生产</option>
                                                <option value="4">团队建设</option>
                                                <option value="5">接待客户</option>
                                                <option value="6">其他</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="layui-form-item">
                                        <label class="layui-form-label required">备注：</label>
                                        <div class="layui-input-block">
                                            <textarea placeholder="简述用车事由" id="remarks" lay-verify="required"
                                                      class="layui-textarea"></textarea>
                                        </div>
                                    </div>


                                    <div class="layui-form-item">
                                        <div class="layui-input-block">
                                            <button class="layui-btn" lay-submit lay-filter="formStep">
                                                &emsp;下一步&emsp;
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div>
                                <form class="layui-form" style="margin: 0 auto;max-width: 460px;padding-top: 40px;">
                                    <div class="layui-form-item">
                                        <label class="layui-form-label required">申请车型：</label>
                                        <div class="layui-input-block">
                                            <select lay-verify="required" id="car_type">
                                                <option value="1" selected>商务车</option>
                                                <option value="2"> 商务VIP车</option>
                                                <option value="3">SUV</option>
                                                <option value="4"> MPV</option>
                                                <option value="5"> 面包车</option>
                                                <option value="6"> 货车</option>
                                                <option value="7">中型客车（10-20人）</option>
                                                <option value="8">大型客车（20人以上）</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="layui-form-item">
                                        <label class="layui-form-label required">用车时间：</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="date" id="startdatetime"
                                                   lay-verify="required||datetime" placeholder="yyyy-MM-dd HH:mm:ss"
                                                   autocomplete="off" class="layui-input" required>

                                        </div>
                                    </div>

                                    <div class="layui-form-item">
                                        <label class="layui-form-label required">随行人数：</label>
                                        <div class="layui-input-block">
                                            <input type="text" id="person_num" name="person_num" placeholder="请填写随行人数"
                                                   class="layui-input" lay-verify="required|number" required>
                                        </div>
                                    </div>

                                    <div class="layui-form-item">
                                        <label class="layui-form-label required">出车地点：</label>
                                        <div class="layui-input-block">
                                            <input type="text" id="start_point" placeholder="请填写出发地点"
                                                   class="layui-input" lay-verify="required|text" required>
                                        </div>
                                    </div>

                                    <div class="layui-form-item">
                                        <label class="layui-form-label required">目的地：</label>
                                        <div class="layui-input-block">
                                            <input type="text" id="end_point" placeholder="请填写出车目的地" class="layui-input"
                                                   lay-verify="required||text" required>
                                        </div>
                                    </div>

                                    <div class="layui-form-item">
                                        <div class="layui-input-block">
                                            <button type="button" class="layui-btn layui-btn-primary pre">上一步</button>
                                            <button class="layui-btn" lay-submit lay-filter="submitBtn">
                                                &emsp;确认申请&emsp;
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div>
                                <div style="text-align: center;margin-top: 90px;">
                                    <i class="layui-icon layui-circle"
                                       style="color: white;font-size:30px;font-weight:bold;background: #52C41A;padding: 20px;line-height: 80px;">&#xe605;</i>
                                    <div style="font-size: 24px;color: #333;font-weight: 500;margin-top: 30px;">
                                        申请成功
                                    </div>
                                    <div style="font-size: 14px;color: #666;margin-top: 20px;">待管理员审批</div>
                                </div>
                                <div style="text-align: center;margin-top: 50px;">
                                    <button class="layui-btn next">继续申请</button>
                                    <button class="layui-btn layui-btn-primary" onclick="window.location.href='http://localhost:8080/CompanyCarManagementSystem_war_exploded/jsp/empl/employee_index.jsp#/../../jsp/empl/current_apply.jsp'">查看当前申请</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div style="color: #666;margin-top: 30px;margin-bottom: 40px;padding-left: 30px;">
                        <h3>说明</h3><br>
                        <h4>申请车型</h4>
                        <p>请按需申请所需车型 </p>
                        <br><h4>申请事由</h4>
                        <p>请简述用车事由 便于部门管理员进行审批以及车辆管理员派单</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script>

    layui.use(['form', 'step', 'laydate'], function () {
        var $ = layui.$,
            form = layui.form,
            step = layui.step,
            laydate = layui.laydate;
        /**
         * 初始化表单，要加上，不然刷新部分组件可能会不加载
         */
        form.render();
        //日期
        laydate.render({
            elem: '#datetime',
            type: 'datetime'
        });
        laydate.render({
            elem: '#startdatetime',
            type: 'datetime'
        });
        step.render({
            elem: '#stepForm',
            filter: 'stepForm',
            width: '100%', //设置容器宽度
            stepWidth: '750px',
            height: '500px',
            stepItems: [{
                title: '请填写基本信息'
            }, {
                title: '请填写用车信息'
            }, {
                title: '提交'
            }]
        });


        form.on('submit(formStep)', function (data) {
            step.next('#stepForm');
            return false;
        });

        form.on('submit(submitBtn)', function () {
            step.next('#stepForm');
            var apply_type = $('#apply_type').val();
            var remarks = $('#remarks').val();
            var car_type = $('#car_type').val();
            var start_date = $('#startdatetime').val();
            var person_num = $('#person_num').val();
            var start_location = $('#start_point').val();
            var destination = $('#end_point').val();
            var username = $('#username').val();
            // 发送Ajax请求
            $.ajax({
                type: "POST",
                url: "../../ApplyCar",
                dataType: 'json',
                data: {
                    apply_type: apply_type,
                    remarks: remarks,
                    car_type: car_type,
                    start_date: start_date,
                    person_num: person_num,
                    start_location: start_location,
                    destination: destination,
                    username: username,
                }
            });
            return false;
        });

        $('.pre').click(function () {
            step.pre('#stepForm');
        });

        $('.next').click(function () {
            //继续申请时清空之前的填写记录
            $('#remarks').val('');
            $('#car_type').val('');
            $('#startdatetime').val('');
            $('#person_num').val('');
            $('#start_point').val('');
            $('#end_point').val('');
            step.next('#stepForm');
        });
    })
</script>
</body>
</html>
