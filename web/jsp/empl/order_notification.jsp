<%@ page import="modle.Bean.Employee" %><%--
  Created by IntelliJ IDEA.
  User: Tsuki
  Date: 2023-04-28
  Time: 23:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>信息通知</title>
</head>
<body>
<%
    Employee employee = (Employee) session.getAttribute("employee");
%>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main">

        <script type="text/html" id="toolbarDemo">
            <div class="layui-btn-container">
                <button class="layui-btn layui-btn-sm layui-btn-danger data-delete-btn" lay-event="deleteReaded">
                    一键删除已读
                </button>
            </div>
        </script>

        <table class="layui-hide" id="orderNotificationTable" lay-filter="currentTableFilter"></table>

        <script type="text/html" id="currentTableBar">
            <a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit" lay-event="viewOrder">查看</a>
            <a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete" lay-event="delete">删除</a>
        </script>

    </div>
</div>

<script>
    layui.use(['form', 'table', 'miniPage', 'element'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            miniPage = layui.miniPage;

        var employeeID = <%= employee.getEmployeeID()%>;
        $.ajax({
            url: '../../OrderNotification',
            method: 'POST',
            dataType: 'json',
            data: {
                employeeID: employeeID,
            },
            success: function (res) {
                // 渲染表格
                table.render({
                    elem: '#orderNotificationTable',
                    data: res.data,
                    toolbar: '#toolbarDemo',
                    defaultToolbar: ['filter', 'exports', 'print', {
                        title: '提示',
                        layEvent: 'LAYTABLE_TIPS',
                        icon: 'layui-icon-tips'
                    }],
                    cols: [[
                        {type: "checkbox", width: 80},
                        {field: 'order_id', width: 120, title: '订单编号', sort: true},
                        {field: 'start_date', width: 180, title: '出车时间', sort: true},
                        {field: 'start_location', width: 150, title: '出车地点', sort: true},
                        {field: 'driver_name', width: 100, title: '司机'},
                        {field: 'car_type', width: 100, title: '车型', minWidth: 120},
                        {field: 'car_lpn', width: 150, title: '车牌号', minWidth: 120},
                        {
                            field: 'status', width: 120, title: '状态', sort: true, templet: function (d) {
                                if (d.status == 0) {
                                    return '未读&nbsp;<span class="layui-badge-dot layui-bg-red"></span>';
                                } else {
                                    return '已读';
                                }
                            }
                        },
                        {
                            title: '操' +
                                '作', minWidth: 150, toolbar: '#currentTableBar', align: "center"
                        }
                    ]],
                    //分页数
                    limits: [10, 15, 20, 25, 50, 100],
                    limit: 10,
                    page: true,
                    skin: 'line'
                });
            },
            error: function (err) {
                console.log(err);
            }
        });

        /**
         * toolbar事件监听
         */
        table.on('toolbar(currentTableFilter)', function (obj) {
            if (obj.event === 'deleteReaded') {
                var checkStatus = table.checkStatus('currentTableId'); // 获取已选中的行数据
                var selectedData = checkStatus.data;
                var deleteData = selectedData.filter(function (item) {
                    return item.status == 1; // 只取 status 属性值为 1 的行数据，即已读状态的数据
                });
                if (deleteData.length > 0) {
                    // 如果存在符合条件的行数据，则弹出确认框
                    layer.confirm('您确定要删除这 ' + deleteData.length + ' 条记录吗？', {icon: 3, title: '警告'}, function (index) {
                        for (var i = 0; i < deleteData.length; i++) {
                            obj.del(deleteData[i].id);
                            // 使用 obj.del() 方法删除一行数据
                        }
                        layer.close(index); // 关闭对话框
                        // 重载表格，使删除后的数据得到更新
                        table.reload('currentTableId');
                    });
                } else { // 如果不存在符合条件的行数据，则弹出提示框
                    layer.msg('没有可删除的数据。');
                }
            }
        });


        //监听表格复选框选择
        table.on('checkbox(currentTableFilter)', function (obj) {
            console.log(obj)
        });

        table.on('tool(currentTableFilter)', function (obj) {
            var data = obj.data;
            if (obj.event === 'viewOrder') {


                var content = miniPage.getHrefContent('page/table/add.html');
                var openWH = miniPage.getOpenWidthHeight();

                var index = layer.open({
                    title: '订单明细',
                    type: 1,
                    shade: 0.2,
                    maxmin: true,
                    shadeClose: true,
                    area: [openWH[0] + 'px', openWH[1] + 'px'],
                    offset: [openWH[2] + 'px', openWH[3] + 'px'],
                    content: "订单具体内容 提示用户司机以及车辆的信息，并提醒用户做好准备前往出车地点等待"
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
                return false;
            } else if (obj.event === 'delete') {
                layer.confirm('确认删除', function (index) {
                    obj.del();
                    layer.close(index);
                });
            }
        });


    });


</script>
</body>
</html>

