<%--
  Created by IntelliJ IDEA.
  User: Tsuki
  Date: 2023-05-26
  Time: 23:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>申请表详细信息</title>

    <style>
        td {
            text-align: center;
        }
        #tablehead{
            text-align: center;
        }
    </style>
</head>
<body>
<h1 id="tablehead">用 车 申 请 表</h1>
<table class="layui-table" lay-size="lg">
    <tbody>
    <tr>
        <td style="width: 100px">申请人</td>
        <td style="width: 100px" id="real_name"></td>
        <td style="width: 100px">联系电话</td>
        <td style="width: 100px" id="tel"></td>
        <td style="width: 100px">所在部门</td>
        <td style="width: 100px" id="dept"></td>
    </tr>
    <tr>
        <td style="width: 100px">申请类型</td>
        <td style="width: 100px" id="apply_type"></td>
        <td style="width: 100px">申请车型</td>
        <td style="width: 100px" id="car_type"></td>
        <td style="width: 100px">随行人数</td>
        <td style="width: 100px" id="person_num"></td>

    </tr>
    <tr>
        <td>申请时间</td>
        <td id="apply_date"></td>
        <td>用车时间</td>
        <td id="start_date"></td>
        <td>申请状态</td>
        <td id="status"></td>
    </tr>
    <tr>
        <td>出车地点</td>
        <td colspan="5" id="start_location"></td>

    </tr>
    <tr>
        <td>目的地</td>
        <td colspan="5" id="destination"></td>

    </tr>
    <tr>
        <td>申请备注</td>
        <td colspan="5" id="remarks"></td>

    </tr>
    <tr>
        <td rowspan="2">备注</td>
        <td colspan="5" >
            1、公司车辆外出需办理用车申请表，经批准后方可外出
            2、公司借给申请人用车，申请借车人为第一责任人，不允许转借给其他人使用，若要借给他人使用。责任人要承担全部责任。
        </td>
    </tr>
    <tr></tr>
    </tbody>
</table>
<script>
    $(function () {
        // 读取全局变量中的数据对象
        var data = window.parent.applycarInfo;
        $('#tel').text(data.tel);
        $('#dept').text(data.dept);
        $('#real_name').text(data.real_name);
        $('#apply_type').text(data.rowdata.apply_type);
        $('#car_type').text(data.rowdata.car_type);
        $('#person_num').text(data.rowdata.person_num);
        $('#apply_date').text(data.rowdata.apply_date);
        $('#start_date').text(data.rowdata.start_date);
        $('#status').text(data.rowdata.status);
        $('#start_location').text(data.rowdata.start_location);
        $('#destination').text(data.rowdata.destination);
        $('#remarks').text(data.rowdata.remarks);
        window.applycarInfo = null;
    });
</script>
</body>
</html>
