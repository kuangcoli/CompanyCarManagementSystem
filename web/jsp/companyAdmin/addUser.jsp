<%--
  Created by IntelliJ IDEA.
  User: Tsuki
  Date: 2023-05-14
  Time: 22:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="layuimini-main">

  <div class="layui-form layuimini-form">

    <div class="layui-form-item">
      <label class="layui-form-label required">选择角色</label>
      <div class="layui-input-block">
        <select  class="layui-select " name="roletype" id="roletype"  lay-verify="required|account"  lay-filter="role-select" autocomplete="off">
          <option value=""> 选择创建的用户角色</option>
          <option value="employee">员工</option>
          <option value="driver">驾驶员</option>
          <option value="vehicleManager">车辆管理员</option>
          <option value="deptAdmin">部门管理员</option>
          <option value="companyAdmin">公司管理员</option>
        </select>
      </div>
    </div>

    <div class="layui-form-item">
      <label class="layui-form-label required">用户名</label>
      <div class="layui-input-block">
        <input type="text" name="username" id="username" lay-verify="required" lay-reqtext="用户名不能为空" placeholder="请输入用户名" value="" class="layui-input">
        <tip>填写自己管理账号的名称。</tip>
      </div>
    </div>

    <div class="layui-form-item">
      <label class="layui-form-label required">密码</label>
      <div class="layui-input-block">
        <input type="password" name="password" required lay-verify="required" placeholder="请输入密码" autocomplete="off" class="layui-input">
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label required">确认密码</label>
      <div class="layui-input-block">
        <input type="password" name="confrim_password" required lay-verify="required" placeholder="请再次输入密码" autocomplete="off" class="layui-input">
      </div>
    </div>

    <div class="layui-form-item">
      <label class="layui-form-label required">姓名</label>
      <div class="layui-input-block">
        <input type="text" name="real_name"   placeholder="请输入姓名" value="" class="layui-input">
      </div>
    </div>

    <div class="layui-form-item">
      <label class="layui-form-label required">手机</label>
      <div class="layui-input-block">
        <input type="tel" name="tel" lay-verify="required|phone" autocomplete="off" lay-reqtext="请填写手机号" lay-affix="clear" class="layui-input demo-phone">
      </div>
    </div>



    <div class="layui-form-item">
      <div class="layui-input-block">
        <button type="submit" class="layui-btn layui-btn-normal" lay-submit lay-filter="saveBtn">确认保存</button>
      </div>
    </div>
  </div>
</div>
<script>
  layui.use(['form', 'table'], function () {
    var form = layui.form,
            layer = layui.layer,
            table = layui.table,
            $ = layui.$;

    /**
     * 初始化表单，要加上，不然刷新部分组件可能会不加载
     */
    form.render();

    // 当前弹出层，防止ID被覆盖
    var parentIndex = layer.index;

    //监听提交
    form.on('submit(saveBtn)', function () {
        var roletype = $('#roletype').val();
        var password = $('input[name="password"]').val();
        var confrimpassword = $('input[name="confrim_password"]').val();
        var real_name = $('input[name="real_name"]').val();
        var tel = $('input[name="tel"]').val();
        var username =$('#username').val();
        // 发送Ajax请求
        $.ajax({
          type: "POST",
          url: "../../AddUser",
          dataType: 'json',
          data: {
            real_name: real_name,
            password :password,
            confrim_password :confrimpassword,
            roletype :roletype,
            tel: tel,
            username :username,
          },
          success: function (res) {
            table.reload('userTable',{
              data:res.data,
            });
          },
          error: function () {
            console.log('更新失败');
          }
        });

      layer.close(parentIndex);
      });
      return false;
  });
</script>
</body>
</html>
