<%--
  Created by IntelliJ IDEA.
  User: Tsuki
  Date: 2023-05-11
  Time: 15:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>用户列表</title>
</head>
<body>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main">
<%--        表格上方搜索功能--%>
        <fieldset class="table-search-fieldset">
            <legend>搜索信息</legend>
            <div style="margin: 10px 10px 10px 10px">
                <form class="layui-form layui-form-pane" action="">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">用户名</label>
                            <div class="layui-input-inline">
                                <input type="text" name="username" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <label class="layui-form-label">用户类型</label>
                            <div class="layui-input-inline">
                                <select  class="layui-select " name="roletype" id="selection"   lay-filter="role-select" autocomplete="off">
                                    <option value=""> 选择角色类型</option>
                                    <option value="employee">员工</option>
                                    <option value="driver">驾驶员</option>
                                    <option value="vehicleManager">车辆管理员</option>
                                    <option value="deptAdmin">部门管理员</option>
                                    <option value="companyAdmin">公司管理员</option>
                                </select>
                            </div>
                        </div>

                        <div class="layui-inline">
                            <button type="submit" lay-submit class="layui-btn layui-btn-primary"  lay-submit lay-filter="data-search-btn"><i class="layui-icon"></i> 搜 索</button>
                        </div>
                    </div>
                </form>
            </div>
        </fieldset>

<%--        表头工具条--%>
        <script type="text/html" id="toolbar">
            <div class="layui-btn-container">
                <button class="layui-btn layui-btn-normal layui-btn-sm data-add-btn" lay-event="add"> 添加用户 </button>
            </div>
        </script>

        <table class="layui-hide" id="userTable" lay-filter="currentTableFilter"></table>
        <script type="text/html" id="currentTableBar">
            <a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit" lay-event="edit">编辑</a>
            <a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete" lay-event="delete">删除</a>
        </script>

    </div>
</div>

<script>

    layui.use(['form', 'table', 'miniPage', 'element'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            miniPage = layui.miniPage;
        $.ajax({
            url: '../../UserInfo',
            method: 'POST',
            dataType: 'json',
            success: function(res){
                // console.log(res.data); // 在控制台中输出res.data内容
        // 渲染表格
        table.render({
            elem: '#userTable',
            data: res.data,
            // url: '../../api/userInfo.json',
            // 表头工具条  这里是添加用户
            toolbar: '#toolbar',
            //默认工具条 筛选  导出  打印
            defaultToolbar: ['filter', 'exports', 'print', {
                title: '提示',
                layEvent: 'LAYTABLE_TIPS',
                icon: 'layui-icon-tips'
            }],
            cols: [[
                {type: "checkbox", width: 100},
                {field: 'username', width: 120, title: '用户名', sort: true},
                {field: 'real_name', width: 120, title: '姓名'},
                {field: 'tel', width: 180, title: '用户电话'},
                {field: 'roletype', width: 160, title: '用户类型', sort: true},
                {field: 'regist_date', width: 220, title: '注册时间', sort: true},
                {
                    title: '操' +
                        '作', minWidth: 150, toolbar: '#currentTableBar', align: "center"
                }
            ]],
            //分页数
            limits: [10, 15, 20, 25, 50, 100],
            limit: 10,
            page: true,
            skin: 'line'
        });
            },
            error: function(err){
                console.log(err);
            }
        });

        /**
         * toolbar事件监听
         */
        table.on('toolbar(currentTableFilter)', function (obj) {
            if (obj.event === 'add') {   // 监听添加操作
                var content = miniPage.getHrefContent('addUser.jsp');
                var openWH = miniPage.getOpenWidthHeight();
                var index = layer.open({
                    title: '添加用户',
                    type: 1,
                    shade: 0,
                    maxmin:true,
                    shadeClose: true,
                    area: [openWH[0] + 'px', openWH[1] + 'px'],
                    offset: [openWH[2] + 'px', openWH[3] + 'px'],
                    content: content,
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });

            }
        });



        //监听表格复选框选择
        table.on('checkbox(currentTableFilter)', function (obj) {
            console.log(obj)
        });

        table.on('tool(currentTableFilter)', function (obj) {
            var data = obj.data;
            if (obj.event === 'edit') {
                window.currentRowData = data;
                var content = miniPage.getHrefContent('editUser.jsp' );
                var openWH = miniPage.getOpenWidthHeight();
                var index = layer.open({
                    title: '编辑用户',
                    type: 1,
                    shade: 0,
                    maxmin: true,
                    shadeClose: true,
                    area: [openWH[0] + 'px', openWH[1] + 'px'],
                    offset: [openWH[2] + 'px', openWH[3] + 'px'],
                    content: content,
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
                return false;
            } else if (obj.event === 'delete') {
                layer.confirm('确认删除', function (index) {
                    $.ajax({
                        url: "../../DeleteUser",
                        type: "POST",
                        dataType: 'json',
                        data: {username: data.username},
                        success: function (res) {
                            table.reload('userTable',{
                                data:res.data,
                            });
                        }
                    });
                    layer.close(index);
                });
            }
        });
        form.on('submit(data-search-btn)', function(data){
            var field = data.field; // 获得表单字段
            // 执行 AJAX 请求
            $.ajax({
                type: "POST",
                url: "../../UserInfo",
                dataType: 'json',
                data: {
                    username: field.username,
                    roletype: field.roletype,
                },
                success: function(res) {
                    table.reload('userTable',{
                        data:res.data,
                    });

                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log("AJAX ERROR:", textStatus, errorThrown);
                    layer.msg('搜索失败');
                }
            });

            return false; // 阻止默认 form 跳转
        });

    });


</script>
</body>
</html>
