<%--
  Created by IntelliJ IDEA.
  User: Tsuki
  Date: 2023-05-14
  Time: 17:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="layuimini-main">
    <div class="layui-form layuimini-form">
        <div class="layui-form-item">
            <label class="layui-form-label">用户名</label>
            <div class="layui-input-block">
                <input type="text"  id="username" name="username"  lay-reqtext="用户名不能为空" readonly  value="" class="layui-input">
                <tip>管理账号的名称。</tip>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label required">姓名</label>
            <div class="layui-input-block">
                <input type="text"  id="real_name" name="real_name" lay-verify="required"   value="" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label required">电话号码</label>
            <div class="layui-input-block">
                <input type="number" id="tel"  name="tel"  lay-verify="required" lay-reqtext="手机不能为空" placeholder="请输入手机" value="" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">用户类型</label>
            <div class="layui-input-block">
                <input type="text" id="roletype" name="roletype" readonly  value="" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">注册时间</label>
            <div class="layui-input-block">
                <input type="text" id="regist_date" name="regist_date" readonly  value="" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn layui-btn-normal" lay-submit lay-filter="saveBtn">确认保存</button>
            </div>
        </div>
    </div>
</div>
<script>
    layui.use(['form', 'table'], function () {
        var form = layui.form,
            layer = layui.layer,
            table = layui.table,
            $ = layui.$;

        /**
         * 初始化表单，要加上，不然刷新部分组件可能会不加载
         */
        form.render();


        // 当前弹出层，防止ID被覆盖
        var parentIndex = layer.index;

        //监听提交
        form.on('submit(saveBtn)', function (data) {
            var index = layer.alert(JSON.stringify(data.field), {
                title: '最终的提交信息'
            }, function () {
                var real_name = $('input[name="real_name"]').val();
                var tel = $('input[name="tel"]').val();
                var username = $('input[name="username"]').val();
                // 发送Ajax请求
                $.ajax({
                    type: "POST",
                    url: "../../UserEdit",
                    dataType: 'json',
                    data: {
                        real_name: real_name,
                        tel: tel,
                        username :username,
                    },
                    success: function (res) {

                        table.reload('userTable',{
                            data:res.data,
                        });

                    },
                    error: function () {
                        console.log('更新失败');
                    }
                });
                 // 关闭弹出层
                layer.close(index);
                layer.close(parentIndex);
            });
            return false;
        });


    });

    $(function () {
        // 读取全局变量中的数据对象
        var data = window.parent.currentRowData;
        // 将数据对象的属性值设置到对应输入框
        $('input[name="username"]').val(data.username);
        $('input[name="real_name"]').val(data.real_name);
        $('input[name="tel"]').val(data.tel);
        $('input[name="roletype"]').val(data.roletype);
        $('input[name="regist_date"]').val(data.regist_date);
        window.currentRowData = null;
    });



</script>
</body>
</html>
