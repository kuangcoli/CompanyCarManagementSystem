
<%--
  Created by IntelliJ IDEA.
  User: Tsuki
  Date: 2023-04-15
  Time: 21:45
  To change this template use File | Settings | File Templates.
--%>
<%--指定页面语言为java 设置jsp编码为utf-8 --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>登陆</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content ="telephone=no">
    <link rel="stylesheet" href="./lib/layui-v2.8.1/layui/css/layui.css" media="all">

    <link rel="stylesheet" href="./css/login.css">
    <link rel="stylesheet" href="./lib/layer/skin/layer.css">
    <script src="./lib/jquery-3.4.1/jquery-3.4.1.min.js" charset="utf-8"></script>
    <script src="./lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
    <script src="./lib/jq-module/jquery.particleground.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="./lib/layer/layer.js"></script>
        <!--[if lt IE 9]>
        <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
        <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    <style>
        html, body {
            width: 100%;
            height: 100%;
            overflow: hidden
        }

        body {
            margin: 0;
            height: 100%;
            background: #2171BB;
            background: url(./img/login.png) no-repeat;
            background-size: 100% 100%;
        }

        body:after {
            content: '';
            background-repeat: no-repeat;
            background-size: cover;
            -webkit-filter: blur(3px);
            /*-moz-filter: blur(3px);*/
            /*-o-filter: blur(3px);*/
            -ms-filter: blur(3px);
            filter: blur(3px);
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: -1;
        }

        .layui-container {
            width: 100%;
            height: 100%;
            overflow: hidden;
        }

        .admin-login-background {
            width: 360px;
            height: 400px;
            position: absolute;
            left: 75%;
            top: 40%;
            margin-left: -180px;
            margin-top: -100px;
        }
        .form-label{
            float: right;
            display: block;
            padding: 9px 15px;
            width: 80px;
            font-weight: 400;
            line-height: 20px;
            text-align: right;
        }


        .captcha {
            width: 100%;
            display: inline-block;
        }

        .captcha-img {
            display: inline-block;
            width: 34%;
            float: right;
        }

        .captcha-img img {
            height: 34px;
            border: 1px solid #e6e6e6;
            height: 36px;
            width: 100%;
        }
        /* 	验证码 隐藏滚动条 */
        .imgcode{
            width: 95px;
            position: absolute;
            right: 0;
            top: 0px;
            cursor: pointer;
            height: 43px;
            font-size: 20px;
            text-align: center;
            line-height: 40px;
        }
        ::-webkit-scrollbar{
            display:none;
        }
    </style>

</head>
<body onload="changeImg()" >



<div class="layui-container">
    <div class="admin-login-background">
        <div class="layui-form login-form">
            <form class="layui-form" action="login" method="post">
                <div class="layui-form-item logo-title">
                    <h1>CCMS后台登录</h1>
                </div>

                <div class="layui-form-item">
                    <select  class="layui-select " name="roletype" id="selection" lay-verify="required|account"  lay-filter="role-select" autocomplete="off">
                        <option value=""> 选择登录角色</option>
                        <option value="employee">员工</option>
                        <option value="driver">驾驶员</option>
                        <option value="vehicleManager">车辆管理员</option>
                        <option value="deptAdmin">部门管理员</option>
                        <option value="companyAdmin">公司管理员</option>
                    </select>


                </div>

                <div class="layui-form-item">
                    <label class="layui-icon layui-icon-username" for="username"></label>
                    <input type="text" name="username" lay-verify="required|account" placeholder="账号"
                           autocomplete="off" class="layui-input" id="username">
                </div>
                <div class="layui-form-item">
                    <label class="layui-icon layui-icon-password" ></label>
                    <input type="password" name="password" lay-verify="required|password" placeholder="密码"
                           autocomplete="off" class="layui-input">
                </div>

                <div class="layui-form-item">
                    <label class="layui-icon layui-icon-vercode" ></label>
                    <input type="text" name="inputcode" lay-verify="required|captcha" placeholder="输入验证码"
                           autocomplete="off" class="layui-input verification captcha" id="verify">
                    <span id="code" class="imgcode"  ></span>
                    <input type="hidden" name="code" id="codeInput">
                </div>


                <div class="layui-form-item">
                    <input type="checkbox" name="rememberMe" value="true" lay-skin="primary" title="记住密码">

                    <a class="form-label " id="register-btn" onclick="openRegister()"> 注册新用户</a>
                </div>

                <div class="layui-form-item">
                    <button class="layui-btn layui-btn layui-btn-normal layui-btn-fluid" lay-submit=""
                            lay-filter="login" type="submit" id="login">登 入
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<%
    String errorMsg = (String) request.getAttribute("errorMsg");
    if (errorMsg != null && !"".equals(errorMsg)) {
%>
<script>
    $(function () {
        layer.msg("<%= errorMsg %>");
    });
</script>
<%
    }
%>


<script>
    function openRegister() {
        layer.open({
            type: 1,
            title: '用户注册',
            content: '<form class="layui-form" action="RegisterUser" method="post">'
                + '<div class="layui-form-item">'
                + '<label class="layui-form-label required">注册角色:</label>'
                + '<div class="layui-input-block">'
                + '<select class="layui-select " name="roletype" id="selection" lay-verify="required|account"  lay-filter="role-select" autocomplete="off">'
                + '<option value="">请选择角色</option>'
                + '<option value="employee">员工</option>'
                + '<option value="driver">驾驶员</option>'
                + '</select></div></div>'
                + '<div class="layui-form-item">'
                + '<label class="layui-form-label required">用户名:</label>'
                + '<div class="layui-input-block">'
                + '<input type="text" name="username" required lay-verify="required" placeholder="请输入用户名" autocomplete="off" class="layui-input">'
                + '</div></div>'
                + '<div class="layui-form-item">'
                + '<label class="layui-form-label required">密码:</label>'
                + '<div class="layui-input-block">'
                + '<input type="password" name="password" required lay-verify="required" placeholder="请输入密码" autocomplete="off" class="layui-input">'
                + '</div></div>'
                + '<div class="layui-form-item">'
                + '<label class="layui-form-label required">确认密码:</label>'
                + '<div class="layui-input-block">'
                + '<input type="password" name="confrim_password" required lay-verify="required" placeholder="请再次输入密码" autocomplete="off" class="layui-input">'
                + '</div></div>'
                +' <div class="layui-form-item">'
                +'<label class="layui-form-label ">姓名</label>'
                +'  <div class="layui-input-block">'
                +' <input type="text" name="real_name"   placeholder="请输入姓名" value="" class="layui-input">'
                +'</div> </div>'
                +'<div class="layui-form-item">'
                +' <label class="layui-form-label ">手机</label>'
                +' <div class="layui-input-block">'
                +' <input type="number" name="tel"  placeholder="请输入手机" value="" class="layui-input">'
                +'</div>  </div>'
                + '<div class="layui-form-item">'
                + '<div class="layui-input-block">'
                + '<button class="layui-btn" lay-submit lay-filter="formDemo">提交</button>'
                + '<button type="reset" class="layui-btn layui-btn-primary">重置</button>'
                + '</div></div></form>',
            area: ['550px', '500px']
        });
        layui.use('form', function(){
            var form = layui.form;
            form.render();
        });
    }


    layui.use(['form', 'jquery'], function(){
        var $ = layui.jquery;
        var form = layui.form;

        //监听 select 的 change 事件
        form.on('select(role-select)', function(data){
            //获取当前选中的选项值
            var val = data.value;

            switch(val) {
                case "employee":
                    //更改输入框的默认内容
                    $('input[name="username"]').attr("placeholder", "请输入员工账号");
                    $('input[name="password"]').attr("placeholder", "请输入密码");
                    break;
                case "driver":
                    //更改输入框的默认内容
                    $('input[name="username"]').attr("placeholder", "请输入驾驶员账号");
                    $('input[name="password"]').attr("placeholder", "请输入密码");
                    break;

                case "vehicleManager":
                    //更改输入框的默认内容
                    $('input[name="username"]').attr("placeholder", "请输入车辆管理员账号");
                    $('input[name="password"]').attr("placeholder", "请输入密钥");
                    break;

                case "deptAdmin":
                    //更改输入框的默认内容
                    $('input[name="username"]').attr("placeholder", "请输入部门管理员账号");
                    $('input[name="password"]').attr("placeholder", "请输入密钥");
                    break;


                case "companyAdmin":
                    //更改输入框的默认内容
                    $('input[name="username"]').attr("placeholder", "请输入公司管理员账号");
                    $('input[name="password"]').attr("placeholder", "请输入密钥");
                    break;
                default:
                    //更改输入框的默认内容
                    $('input[name="username"]').attr("placeholder", "账号");
                    $('input[name="password"]').attr("placeholder", "密码");
                    break;
            }
        });
    });
    //不加这个复选框样式不会改变
    layui.use('form', function() {
        var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
    });
    //用于验证码
    var code; //声明一个变量用于存储生成的验证码
    document.getElementById("code").onclick = changeImg;
    function changeImg() {
        //alert("换图片");
        var arrays = new Array(
            '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
            'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
            'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
            'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
            'U', 'V', 'W', 'X', 'Y', 'Z'
        );
        code = ''; //重新初始化验证码
        //alert(arrays.length);
        //随机从数组中获取四个元素组成验证码
        for (var i = 0; i < 4; i++) {
            //随机获取一个数组的下标
            var r = parseInt(Math.random() * arrays.length);
            code += arrays[r];
            //alert(arrays[r]);
        }

        document.getElementById('code').innerHTML = code; //将验证码写入指定区域
        document.getElementById('codeInput').value = code;
    }





    layui.use(['form'], function () {
        var form = layui.form,
            layer = layui.layer;

        // 登录过期的时候，跳出ifram框架
        if (top.location != self.location) top.location = self.location;

        // 粒子线条背景
        $(document).ready(function () {
            $('.layui-container').particleground({
                dotColor: '#7ec7fd',
                lineColor: '#7ec7fd'
            });
        });

    });

    $('.imgcode').hover(function() {

        layer.tips("点击更换验证码", '#code', {
            time: 2000,
            tips: [2, "#3c3c3c"]
        });
    });

</script>
</body>
</html>